import React from "react";
import WordList from "./wordslist";
import HowItWorks from "./howitworks"
import 'bootstrap/dist/css/bootstrap.min.css';
import Contact from "./contact";
import Logo from "./logo.png"

import { Navbar } from 'react-bootstrap'

function App() {

  return (
    <>

      <Navbar expand="lg">
        <Navbar.Brand href="#home"><img src={Logo} alt='logo'></img>StudyBeforeRead.com</Navbar.Brand>

      </Navbar>

      


      <ul style={{listStyleType: "none"}}>
        <li style={{marginTop: "5px"}}><HowItWorks/></li>
        <li style={{marginTop: "5px"}}><Contact/></li>
      </ul>
          
          



      <WordList />

    </>

  );
}


export default App;

