import React from 'react';
import { Button } from 'react-bootstrap'

const Contact = () => {
    const [showResults, setShowResults] = React.useState(false)
    
    function toggle() {
        setShowResults(showResults === false ? true : false);
      }


    return (
        <div>

        <Button variant="outline-primary" onClick={toggle}>Email me!</Button>
        { showResults ? <Results /> : null }

      </div>
    )
  }
  
  const Results = () => (
    <div>
      Adrian Zyskowski 
      zyskow.ad@gmail.com 
    </div>
  )
  



export default Contact;