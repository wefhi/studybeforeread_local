import React from 'react';
import { Button } from 'react-bootstrap'

const HowItWorks = () => {
    const [showResults, setShowResults] = React.useState(false)
    
    function toggle() {
        setShowResults(showResults === false ? true : false);
      }


    return (
      <div>

        <Button variant="outline-primary" onClick={toggle}>How does it work?</Button>
        { showResults ? <Results /> : null }

      </div>
    )
  }
  
  const Results = () => (
      <ul style={{listStyleType: "value"}}>
        <li>Study list of the hardest words from the website and never use Google Translator again!</li>
        <li>Simply paste your link. Wait about second or two and get list of words.</li>
        <li>Use the slider to adjust your English proficiency level. The easiest words will appear on the top, the hardest on the bottom</li>
        <li>THE WORDS DATABASE IS NOT COMPLETE (but its learning itself) so sometimes if you don't get all the words you must reload article 2 minutes later again</li>
        <li> the "https://" or "http://" in the link adress is required </li>
      </ul>

  )
  



export default HowItWorks;