import React, { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';
import RangeSlider from 'react-bootstrap-range-slider'

import Iframe from 'react-iframe'

import { Container } from 'react-bootstrap'


import { useForm } from "react-hook-form";



function WordList() {
    const [data, setData] = useState({ words: [] });
    const [value, setValue] = useState(3);
    const [articleName, setArticleName] = useState("link");
    const [articleId, setArticleId] = useState(1);

    function onSubmitForm(formData) {
        alert("You typed: " + formData.articleUrl);
        addArticle(formData.articleUrl)
    }

    function MyForm() {
        const { register, handleSubmit } = useForm();
        return (
            <form onSubmit={handleSubmit(onSubmitForm)}>
                <label>
                    <input type="text" name="articleUrl" ref={register} placeholder="Paste your article here" />
                </label>
                <input type="submit" value="Study Before Read!" />
            </form>
        );
    }



    async function addArticle(link) {
        const result = await axios.post('http://127.0.0.1:8000/api/articles/', {
            link: `${link}`
        });
        console.log(result.data.result)
        setArticleId(result.data.result)

        const get_result = await axios.get(`http://127.0.0.1:8000/api/articles/${articleId}`);
        console.log(get_result.data)
        setData(get_result.data);

    }


    useEffect(() => {
        (async () => {
            try {
                const result = await axios.get(
                    `http://127.0.0.1:8000/api/articles/${articleId}`
                );
                console.log(result.data)
                setData(result.data);
                setArticleName(result.data);

            } catch (e) { }
        })();
    }, [articleId]);


    return (
        <>
            <Container>
                

                            <h5>Current Article: </h5><a href={articleName.link}>{articleName.link}</a>


                            <MyForm />




                            <h5>Choose your English Level: </h5> English proficiency: 2=C1 | 3=B2+ | 4=B1 | 5=A2 | 6,7=A1
                    <RangeSlider
                                value={value}
                                onChange={changeEvent => {
                                    setValue(changeEvent.target.value)

                                }}
                                step={0.5}
                                size={'lg'}
                                min={2}
                                max={7}
                                variant={'dark'}

                            />

               





                {data.words.map(item => (

                    <div key={item.objectID}>

                        {item.advance_level < value && item.advance_level !== 0 && item.definition !== "[]" &&
                            <>
                                <b>{item.name} {item.advance_level}</b>
                                <p>{item.definition}</p>
                            </>
                        }

                    </div>

                ))}



            </Container>


            <div style={{ padding: "2em" }}>
                <Iframe url={articleName.link}
                    width="100%"
                    height="1000px"
                    id="myId"
                    className="myClassname"
                    display="initial"
                    position="relative"
                />
            </div>

        </>
    );
}

export default WordList;