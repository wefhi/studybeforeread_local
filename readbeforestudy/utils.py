import requests
from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib.request



def get_word_definition(word):
    response = requests.get(
        "https://wordsapiv1.p.rapidapi.com/words/{}/definition".format(word),
        headers={'x-rapidapi-host': 'wordsapiv1.p.rapidapi.com',
                 'x-rapidapi-key': '3e028da715msh83cd20a5d585d3fp10eb06jsnc9fdb03dee87'}
    )

    if response.status_code == 200:
        if "definition" in response.json():
            return str(response.json()['definition'])
        else:
            return "xd"
    else:
        return "xd"


def get_word_frequency(word):
    response = requests.get(
        "https://wordsapiv1.p.rapidapi.com/words/{}/frequency".format(word),
        headers={'x-rapidapi-host': 'wordsapiv1.p.rapidapi.com',
                 'x-rapidapi-key': '3e028da715msh83cd20a5d585d3fp10eb06jsnc9fdb03dee87'}
    )

    if response.status_code == 200:
        if "frequency" in response.json():
            return response.json()['frequency']['zipf']
        else:
            return "0"
    else:
        return "0"


class WordList:
    def __init__(self, link):
        html = urllib.request.urlopen(link).read()
        self.list = list(set(interpunction_remover(
            text_from_html(html))))  # cos brzydkiego

    def __getitem__(self, index):
        return self.list[index]


def text_from_html(body):
    text = []
    soup = BeautifulSoup(body, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)
    for word in visible_texts:
        text.extend(word.lower().split())
    text = filter(None, text)

    return set(text)


def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True


def interpunction_remover(wordList):
    list = wordList
    list2 = []
    line = 0
    for e in list:
        e = e.replace('.', '').replace(',', ' ').replace('\n', ' ').replace(
            '\r', ' ').replace(':', ' ').replace('"', ' ').replace('?', ' ')
        e = e.replace('„', '').replace('”', ' ').replace(
            '#', ' ').replace('?', ' ').replace('!', ' ').replace('%', ' ')
        e = e.replace('|', '').replace('©', ' ').replace(
            '(', ' ').replace(')', ' ').replace('!', ' ').replace('%', ' ')
        e = e.replace('1', '').replace('2', ' ').replace(
            '3', ' ').replace('4', ' ').replace('5', ' ').replace('6', ' ')
        e = e.replace('7', '').replace(
            '8', ' ').replace('9', ' ').replace(' ', '').replace('0', '').replace('00', '')
        list2.append(e)
    return list2
