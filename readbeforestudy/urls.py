from django.urls import path, include
from .views import ArticleViewSet, WordViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('articles', ArticleViewSet)
router.register('words', WordViewSet)

urlpatterns = [

    path('', include(router.urls)),

]
