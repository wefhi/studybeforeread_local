from __future__ import absolute_import, unicode_literals
from .celery import app

from .utils import get_word_frequency, get_word_definition, WordList

from .models import Article, Word

# add lacking words to database
@app.task
def fetch_foreign_api_with_celery(article_id, article_link, *args, **kwargs):

    temp_web_list = []

    words = WordList(article_link)
    for w in words.list:
        try:
            word = Word.objects.filter(name=w)[0]

        except:
            temp_web_list.append(w)

    web_list = []
    for w in range(len(temp_web_list)):

        word_definition = get_word_definition(temp_web_list[w])
        word_frequency = get_word_frequency(temp_web_list[w])

        try:
            if(word_definition != 'xd' and word_frequency != 0):
                web_list.append(
                    Word(
                        name=temp_web_list[w],
                        advance_level=word_frequency,
                        definition=word_definition,
                        article=Article.objects.get(id=article_id)
                    )
                )
                print("{}/{} '{}' get from External API".format(w,
                                                                len(temp_web_list), temp_web_list[w]))
            else:
                print(
                    " BAD WORD {} {}/{}".format(temp_web_list[w]), w, len(temp_web_list))
        except:
            print(
                " No definition / Advance level  '{}'".format(temp_web_list[w]))

    Word.objects.bulk_create(
        sorted(web_list, key=lambda x: float(x.advance_level), reverse=True))


@app.task
def delete_with_celery(article_id, article_link, *args, **kwargs):

    words = WordList(article_link)
    for w in words.list:
        try:
            word = Word.objects.filter(name=w)[1]
            
            word.delete()
            print("deleted {}".format(w))
        except:
            print("couldn't delete {}".format(w))
