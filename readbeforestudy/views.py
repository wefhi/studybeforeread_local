
from .models import Article, Word
from .serializers import ArticleSerializer, WordSerializer


from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response


from .tasks import fetch_foreign_api_with_celery, delete_with_celery

from .celery import app

from .utils import WordList


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def create(self, request):
        article = Article.objects.create(link=request.data['link'])
        article.save()
        serializer = ArticleSerializer(article, many=False)

        # create words from database
        db_list = []
        words = WordList(article.link)
        for w in words.list:
            try:
                word = Word.objects.filter(name=w)[0]
                print(word.name)
                db_list.append(
                    Word(
                        name=word.name,
                        advance_level=word.advance_level,
                        definition=word.definition,
                        article=Article.objects.get(id=article.id)))
            except:
                pass

        Word.objects.bulk_create(
            sorted(db_list, key=lambda x: float(x.advance_level), reverse=True))
        print("DATABASE Words added")

        # create the rest of words with WordsAPI
        fetch_foreign_api_with_celery.delay(
            article_id=article.id, article_link=article.link)

        # delete words that are not unique
        delete_with_celery.delay(
            article_id=article.id, article_link=article.link)

        response = {'message': 'Article created ',
                    'result': article.id}
        return Response(response, status=status.HTTP_200_OK)


class WordViewSet(viewsets.ModelViewSet):
    queryset = Word.objects.all()
    serializer_class = WordSerializer
