from __future__ import absolute_import, unicode_literals
from django.conf import settings
import os
from celery import Celery
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'home.settings.base')
app = Celery(
    'readbeforestudy',
    broker='redis://localhost:6379',
    backend='redis://localhost:6379',
    include=['readbeforestudy.tasks', 'readbeforestudy.views']
)

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


if __name__ == 'main':
    app.start()
