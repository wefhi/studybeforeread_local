from django.db import models


class Article(models.Model):
    link = models.CharField(max_length=2000)

    def __str__(self):
        return self.link


class Word(models.Model):
    name = models.CharField(max_length=40)
    advance_level = models.FloatField(default=7.00)
    definition = models.CharField(max_length=6000)
    article = models.ForeignKey(
        Article, on_delete=models.CASCADE, related_name='words')

    def __str__(self):
        return self.name
